#include "ui_QtVTKRenderWindows.h"
#include "QtVTKRenderWindows.h"

#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include "vtkResliceImageViewer.h"
#include "vtkResliceCursorLineRepresentation.h"
#include "vtkResliceCursorThickLineRepresentation.h"
#include "vtkResliceCursorWidget.h"
#include "vtkResliceCursorPolyDataAlgorithm.h"
#include "vtkResliceCursor.h"
#include "vtkImageData.h"
#include "vtkCommand.h"
#include "vtkImageSlabReslice.h"
#include "vtkResliceImageViewerMeasurements.h"
#include "vtkNIFTIImageReader.h"
#include "vtkResliceCursorLineRepresentation.h"
#include "vtkActor.h"
#include "vtkProperty.h"
#include "vtkResliceCursorActor.h"

//----------------------------------------------------------------------------

QtVTKRenderWindows::QtVTKRenderWindows( int vtkNotUsed(argc), char *argv[])
{
  vtkObject::GlobalWarningDisplayOff();
  this->ui = new Ui_QtVTKRenderWindows;
  this->ui->setupUi(this);


  vtkSmartPointer<vtkNIFTIImageReader> reader = vtkSmartPointer<vtkNIFTIImageReader>::New();
  reader->SetFileName("D:/SucabotProject/Test/fourpaneviewer/nnn.nii.gz");
  reader->Update();

  //int imageDims[3];
  //reader->GetOutput()->GetDimensions(imageDims);
  m_input = vtkSmartPointer<vtkImageData>::New();
  m_input->DeepCopy(reader->GetOutput());

  for (int i = 0; i < 3; i++)
    {
    riw[i] = vtkSmartPointer< vtkResliceImageViewer >::New();
  }

  this->ui->view1->SetRenderWindow(riw[0]->GetRenderWindow());
  this->ui->view2->SetRenderWindow(riw[1]->GetRenderWindow());
  this->ui->view3->SetRenderWindow(riw[2]->GetRenderWindow());
  riw[0]->SetupInteractor(this->ui->view1->GetRenderWindow()->GetInteractor());
  riw[1]->SetupInteractor(this->ui->view2->GetRenderWindow()->GetInteractor());
  riw[2]->SetupInteractor(this->ui->view3->GetRenderWindow()->GetInteractor());
 

  for (int i = 0; i < 3; i++)
    {
	  riw[i]->SetInputData(reader->GetOutput());
	  riw[i]->SetSliceOrientation(i);
	  riw[i]->SetResliceModeToAxisAligned();
    }

  this->Compute();

  this->ui->view1->show();
  this->ui->view2->show();
  this->ui->view3->show();


};

void QtVTKRenderWindows::slotExit()
{
  qApp->exit();
}

void QtVTKRenderWindows::Compute()
{
	int extent[6];
	double spacing[3];
	m_input->GetExtent(extent);
	m_input->GetSpacing(spacing);
	
	double thickness[3];
	for (int j = 0; j < 3; j++)
	{
		thickness[j] = extent[j * 2 + 1] * spacing[j] ;
	}

	for (int i = 0; i < 3; i++)
    {
		riw[i]->SetResliceModeToOblique();
		riw[i]->SetThickMode(1);
		vtkImageSlabReslice *thickSlabReslice = vtkImageSlabReslice::SafeDownCast(vtkResliceCursorThickLineRepresentation::SafeDownCast(riw[i]->GetResliceCursorWidget()->GetRepresentation())->GetReslice());
		thickSlabReslice->SetBlendModeToMax();

		std::cout << "thickness: " << thickness[0] << " " << thickness[1] << " " << thickness[2] << endl;
		riw[i]->GetResliceCursor()->SetThickness(thickness);
		riw[i]->GetRenderer()->ResetCamera();
		//riw[i]->GetRenderer()->RemoveViewProp(riw[i]->GetResliceCursorWidget()->EnabledOff());
		riw[i]->GetResliceCursorWidget()->SetManageWindowLevel(false);
	
		//rep->Print(std::cout);
		vtkResliceCursorThickLineRepresentation *rep2 = dynamic_cast<vtkResliceCursorThickLineRepresentation *>(riw[i]->GetResliceCursorWidget()->GetRepresentation());
		rep2->GetResliceCursorActor()->GetCenterlineActor(0)->SetVisibility(false);
		rep2->GetResliceCursorActor()->GetCenterlineActor(1)->SetVisibility(false);
		rep2->GetResliceCursorActor()->GetCenterlineActor(2)->SetVisibility(false);
		rep2->GetResliceCursorActor()->SetDragable(false);
		rep2->GetResliceCursorActor()->GetCenterlineProperty(0)->SetOpacity(0.0);
		rep2->GetResliceCursorActor()->GetCenterlineProperty(1)->SetOpacity(0.0);
		rep2->GetResliceCursorActor()->GetCenterlineProperty(2)->SetOpacity(0.0);
		
		rep2->GetResliceCursorActor()->GetThickSlabProperty(0)->SetOpacity(0.0);
		rep2->GetResliceCursorActor()->GetThickSlabProperty(1)->SetOpacity(0.0);
		rep2->GetResliceCursorActor()->GetThickSlabProperty(2)->SetOpacity(0.0);

		riw[i]->Render();
    }
}

void QtVTKRenderWindows::ResetViews()
{
  // Reset the reslice image views
  for (int i = 0; i < 3; i++)
    {
    riw[i]->Reset();
    }

  //// Render in response to changes.
  this->Render();
}

void QtVTKRenderWindows::Render()
{
  for (int i = 0; i < 3; i++)
    {
    riw[i]->Render();
    }
  this->ui->view3->GetRenderWindow()->Render();
}

